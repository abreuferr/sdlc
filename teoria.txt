#
# Static Application Security Testing (SAST)
#

- white-hat ou white-box testing
- examina o código fonte do software que esta sendo desenvolvido


#
# Dynamic Application Security Testing (DAST)
#

- black-hat ou black-box testing
- examina o aplicativo quando o aplicativo esta sendo executado

#
# Application Security Testing as a Service (ASTaaS)
# 

- combinação dos serviços SAST, DAST, pentest, teste em API

#
# Interactive Application Security Testing (IAST)
#

